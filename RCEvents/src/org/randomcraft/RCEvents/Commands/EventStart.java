package org.randomcraft.RCEvents.Commands;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.randomcraft.RCEvents.RCEvents;
import org.randomcraft.RCEvents.Groups.InChallenge;
import org.randomcraft.RCEvents.Groups.SpecChallenge;

import net.minecraft.server.v1_8_R3.CommandExecute;

public class EventStart extends CommandExecute implements Listener, CommandExecutor{

	private RCEvents plugin;
	public EventStart(RCEvents instance) {
	    plugin = instance;
	}
	
	
	public String prefix = (ChatColor.GRAY + "(" + ChatColor.DARK_AQUA + "RC" + ChatColor.GRAY + ") ");
	public String noperms = (prefix + "You do not have permission!");
	public String args = (prefix + "Invalid Arguments!");
	
	
	public ArrayList<Player> ic = InChallenge.getIC();

	public ArrayList<Player> sc = SpecChallenge.getSC();
	public ArrayList<Player> solved = Solve.getSolved();
	public ArrayList<Player> winner = Solve.getWinner();
	
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("event")) {
			if(sender.hasPermission("randomcraft.events.admin")) {
				if(args.length >= 2) {
					if(args[0].equalsIgnoreCase("start")) {
						
						for (String key : plugin.getConfig().getConfigurationSection("Challenges").getKeys(false)) {
						
						if(args[1].equalsIgnoreCase(key)) {
							
							String event = plugin.getConfig().getString("Challenges." + args[1]);
							
							
							Bukkit.getServer().broadcastMessage(prefix + "A challenge is starting in 1 minute! Do /eventjoin " + plugin.getConfig().getString(event + ".eventlobby.world") + " to join!");
							plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

								@SuppressWarnings("unlikely-arg-type")
								@EventHandler
								public void run(PlayerMoveEvent event) {
									
									Bukkit.getServer().broadcastMessage(prefix + "A challenge has started in " + plugin.getConfig().getString(event + ".eventlobby.world") + "!");
									
									for(Player player : Bukkit.getOnlinePlayers()) {
										if(player.getWorld().equals(event + ".eventlobby.world")) {
											
											ic.add(player);
											
											
											String world = plugin.getConfig().getString(event + ".maplocation.world");
											
											World worldvalue = Bukkit.getServer().getWorld(world);
											float yaw = plugin.getConfig().getInt(event + ".yaw");
											float pitch = plugin.getConfig().getInt(event + ".pitch");
											float x = plugin.getConfig().getInt(event + ".x");
											float y = plugin.getConfig().getInt(event + ".y");					
											float z = plugin.getConfig().getInt(event + ".z");
											
											Location map = new Location(worldvalue, x ,y, z, yaw, pitch);
											player.teleport(map);												
											return;
											
										}
										return;
									}
									return;
								}

								@Override
								public void run() {
									// TODO Auto-generated method stub
									
								}
								
								
								
								
								
								
							}, 1200);
						}else {
							sender.sendMessage(args);
							return true;
						}
						
					}
				}if(args[0].equalsIgnoreCase("end")) {
					for (String key : plugin.getConfig().getConfigurationSection("Challenges").getKeys(false)) {
						if(args[1].equalsIgnoreCase(key)) {
							String event = plugin.getConfig().getString("Challenges." + args[1]);
							Bukkit.getServer().broadcastMessage(prefix + "A challenge is ending in 1 minute!");
							plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

								@SuppressWarnings("unlikely-arg-type")
								@Override
								public void run() {
									
									Bukkit.getServer().broadcastMessage(prefix + "A challenge has ended!");
									
									for(Player player : Bukkit.getOnlinePlayers()) {
										if(player.getWorld().equals(event + ".maplocation.world")) {
											
											ic.remove(player);
											sc.remove(player);
											
											
											player.performCommand("hub");											
											
											return;
										}
											
									}
									
								}
								
								
								
								
								
							}, 1200);
						}else {
							sender.sendMessage(args);
							return true;
						}

					}
				}
			}else {
				sender.sendMessage(args);
				return true;
			}
			
		}else {
			sender.sendMessage(noperms);
			return true;
		}
			}
		return true;
	}
}