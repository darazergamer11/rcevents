package org.randomcraft.RCEvents.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.randomcraft.RCEvents.RCEvents;

import net.minecraft.server.v1_8_R3.CommandExecute;

public class LobbyJoin extends CommandExecute implements Listener, CommandExecutor{

	
	private RCEvents plugin = RCEvents.getPlugin(RCEvents.class);
	public String prefix = (ChatColor.GRAY + "(" + ChatColor.DARK_AQUA + "RC" + ChatColor.GRAY + ") ");
	
	
	
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
					
		if(!(sender instanceof Player)) {
			sender.sendMessage(prefix + "You must be a player to execute this command!");
			if(cmd.getName().equalsIgnoreCase("eventjoin")) {
				if(args.length > 0) {
					if(args[0].equalsIgnoreCase("Challenges." + args[0])) {
					
										
						
						String eventname = plugin.getConfig().getString("Challenges." + args[0]);
						String world = plugin.getConfig().getString(eventname + ".eventlobby.world");
						World worldvalue = Bukkit.getServer().getWorld(world);
						
						
						float yaw = plugin.getConfig().getInt(eventname + ".yaw");
						float pitch = plugin.getConfig().getInt(eventname + ".pitch");
						float x = plugin.getConfig().getInt(eventname + ".x");
						float y = plugin.getConfig().getInt(eventname + ".y");					
						float z = plugin.getConfig().getInt(eventname + ".z");
						
						Location lobby = new Location(worldvalue, x ,y, z, yaw, pitch);
						
						Player player = (Player)sender;

						player.teleport(lobby);							
						
						
						sender.sendMessage(prefix + "You have joined the challenge!");
					
						
						}
					}else {
						return true;
					}
				}else {
					return true;
				}
			}else {
				sender.sendMessage(prefix + "Invalid Arguments");
				return true;
			
		}	
		
		sender.sendMessage(prefix + "hi");
		return true;
	}

}
