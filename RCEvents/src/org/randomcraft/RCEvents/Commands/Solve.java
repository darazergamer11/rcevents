package org.randomcraft.RCEvents.Commands;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.randomcraft.RCEvents.RCEvents;
import org.randomcraft.RCEvents.Groups.InChallenge;
import org.randomcraft.RCEvents.Groups.SpecChallenge;

import net.minecraft.server.v1_8_R3.CommandExecute;

public class Solve extends CommandExecute implements Listener, CommandExecutor{

	private RCEvents plugin = RCEvents.getPlugin(RCEvents.class);
	public String prefix = (ChatColor.GRAY + "(" + ChatColor.DARK_AQUA + "RC" + ChatColor.GRAY + ") ");
	
	
	public static ArrayList<Player> solved = new ArrayList<Player>();
	public static ArrayList<Player> getSolved(){
		return solved;
	}
	
	public static ArrayList<Player> winner = new ArrayList<Player>();
	public static ArrayList<Player> getWinner(){
		return winner;
	}
	
	
	public ArrayList<Player> ic = InChallenge.getIC();

	public ArrayList<Player> sc = SpecChallenge.getSC();

	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		String challenges = plugin.getConfig().getString("Challenges." + args[0]);
		
		if(cmd.getName().equalsIgnoreCase("solve")) {
			if(args.length >= 1) {
				if(args[0].equals(plugin.getConfig().getString(challenges + ".solvedcode"))) {
					
					Player player = (Player)sender;

					ic.remove(sender);
					sc.add((Player) sender);
					solved.add(player);
					
					sender.sendMessage(prefix + "You have succesfully solved the challenge!");
					plugin.getServer().broadcastMessage(prefix + ChatColor.DARK_RED + sender.getName() + ChatColor.RESET + " has succesfully solved the challenge!");
					player.setGameMode(GameMode.CREATIVE);

					if(winner.isEmpty()) {
						winner.add(player);
					}else {
						return false;
					}
				}else {
					sender.sendMessage(prefix + "Incorrect! Try again");
					return false;
				}
			}else {
				sender.sendMessage(prefix + "Invalid Arguments!");
				return true;
			}

		}
		
		
		
		return false;
	}

}
